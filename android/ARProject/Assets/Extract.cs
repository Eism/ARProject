﻿using UnityEngine;
using System.Collections.Generic;
using Vuforia;
using UnityEngine.SceneManagement;


public class Extract : MonoBehaviour
{
    private bool mDataSetsLoaded = false;
    public List<string> mDataSetNames; //replace with your dataset name
    // Update is called once per frame
    private void Start()
    {
        mDataSetNames.Add(SceneManager.GetActiveScene().name + ".dat");
        mDataSetNames.Add(SceneManager.GetActiveScene().name + ".xml");
        VuforiaBehaviour vb = GameObject.FindObjectOfType<VuforiaBehaviour>();
        vb.RegisterVuforiaStartedCallback(LoadDataSet);
    }
    void LoadDataSet()
    {
        ObjectTracker itracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        if (itracker == null) return;
        if (!mDataSetsLoaded)
        {
            string dataSetRootDir = Application.persistentDataPath + "/QCAR/";
            foreach (var datasetName in mDataSetNames)
            {
                DataSet dataSet = itracker.CreateDataSet();
                if (dataSet.Load(dataSetRootDir + datasetName, VuforiaUnity.StorageType.STORAGE_ABSOLUTE))
                    itracker.ActivateDataSet(dataSet);
            }
            mDataSetsLoaded = true;
        }
    }
}