﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class DLC : MonoBehaviour
{

    [Header("UI Stuff")]
    public Image image;
    public Image background;
    public Text nameText;
    public Button btShowScene;

    public string urlSceneLoad;

    public string sceneName;
    public string filePath;

    public void Init(string Name, string sceneName, string iconePath)
    {
        nameText.text = Name;
        this.sceneName = sceneName;

        byte[] fileData = File.ReadAllBytes(iconePath);
        var tex = new Texture2D(2, 2);
        tex.LoadImage(fileData);
        image.material.mainTexture = tex;

        filePath = UnityDLC.dlcPath + sceneName;

        if (File.Exists(filePath))
            background.color = new Color32(0, 191, 57, 255);
        else background.color = new Color32(0, 120, 191, 255);
    }

    
}
