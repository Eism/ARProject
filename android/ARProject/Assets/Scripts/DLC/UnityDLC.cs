﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class ButtonExtension
{
    public static void AddEventListner<T>(this Button button, T param, Action<T> OnClick)
    {
        button.onClick.AddListener(delegate {
            OnClick(param);
        });
    }
}

public class UnityDLC : MonoBehaviour
{
    [Header("Список всех сцен")]
    public GameObject allSceneContainer;
    public GameObject allSceneItem;

    [Header("Список моих сцен")]
    public GameObject mySceneContainer;
    public GameObject mySceneItem;

    [Header("Ссылки для загрузок ссцен и дополнительных файлов")]
    public string allSceneUrl;
    public static string dlcPath;
    public string dlcIconePath;
    public string databasePath;
    public string newUserUrl;
    public string mySceneUrl;

public JSONObject JO;

    [Header("UI SceneInfo")]
    public GameObject loadSceneParent;
    public GameObject loadSceneContainer;

    public GameObject progressBarContainer;

    static List<AssetBundle> assetBundles = new List<AssetBundle>();
    static List<string> sceneNames = new List<string>();

    public static UnityDLC Instance { get; private set; }
    private DLC dlc = null;

    private void Start()
    {
        Instance = this;
        dlcPath = (Application.platform == RuntimePlatform.Android ? Application.persistentDataPath : Application.dataPath) + "/DLC/";

        var ds = DataService.Instance;
        ds.DatabaseName = "scenesDatabase.db";

        if (!ds.CheckDB())
        {
            ds.InitDB();
            ds.CreateDB();

            GetScenes(1483228800, SystemInfo.deviceUniqueIdentifier);//1.1.2017 00:00:
        }
        else
        {
            ds.InitDB();
            Init();
            ShowDLC();
            ShowMyScenesDLC();
        }

    }

    public void RefreshLists()
    {
        var ds = DataService.Instance;
        ds.CreateDB();
        GetScenes(1483228800, SystemInfo.deviceUniqueIdentifier);//1.1.2017 00:00:
    }

    public void GetScenes(int Date,string UniqueIdentifier)
    {
        StartCoroutine(GetScenesFromServer(Date, UniqueIdentifier));
    }

    IEnumerator GetScenesFromServer(int Date, string UniqueIdentifier)
    {
        WWWForm form = new WWWForm();
        form.AddField("Date", Date.ToString());
        form.AddField("UniqueIDDevice", UniqueIdentifier);
        ProgressBar pB = CreateProgressBarContainer();
        using (WWW www = new WWW(allSceneUrl, form))
        {
            while (!www.isDone)
            {
                pB.currentAmount += www.progress * 100 / 2;
                yield return null;
            }
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("Log:" + www.error);
                pB.SetErrorLog("Проверьте настройки интернета");
                pB.Destroy();
                //*************** Вывести ошибку
                yield break;
            }

            if (www.text == "Error DATA")
            {
                Debug.Log("Log: Error DATA");
                pB.SetErrorLog("Ошибка загрузки");
                pB.Destroy();
                //*************** Вывести ошибку
                yield break;
            }

            JO = new JSONObject(www.text);
            DataService ds = DataService.Instance;
            foreach (var json in JO.list)
            {
                var data = json.ToDictionary();

                WWWForm formIcone = new WWWForm();
                formIcone.AddField("SceneName", data["SceneName"]);

                WWW wwwIcone = new WWW(dlcIconePath, formIcone);
                while (!wwwIcone.isDone)
                {
                    pB.currentAmount += www.progress * 100 / 2;
                    yield return null;
                }

                if (!string.IsNullOrEmpty(wwwIcone.error))
                {
                    Debug.Log("Log: Error Data Icone");
                    pB.SetErrorLog("Ошибка загрузки");
                    pB.Destroy();
                    yield break;
                }
                if (wwwIcone.text == "Error DATA")
                {
                    Debug.Log("Log: Error DATA");
                    pB.SetErrorLog("Ошибка загрузки");
                    pB.Destroy();
                    //*************** Вывести ошибку
                    yield break;
                }


                if (!Directory.Exists(dlcPath + "Icone/"))
                {
                    Directory.CreateDirectory(dlcPath + "Icone/");
                }
                string dlcName = data["SceneName"].Replace(".dlc", "");
                string Icone = dlcPath + "Icone/" + dlcName + ".png";

                File.WriteAllBytes(Icone, wwwIcone.bytes);

                ds.CreateScene(data["SceneName"], data["Name"],
                    Icone, 0, Convert.ToInt32(data["isAvailable"]), data["Description"],
                    Convert.ToInt32(data["Price"]), Convert.ToInt32(data["Date"]));

                pB.Destroy();
            }
            Init();
            ShowDLC();
            ShowMyScenesDLC();
        }

    }    

    public void Init()
    {
        StartCoroutine(LoadAssets());
    }

    IEnumerator LoadAssets()
    {
        Debug.Log("Log: Init()");
        if (!Directory.Exists(dlcPath))
        {
            Directory.CreateDirectory(dlcPath);
        }

        foreach (AssetBundle bundle in assetBundles)
        {
            bundle.Unload(true);
        }

        assetBundles.Clear();
        sceneNames.Clear();

        DataService ds = DataService.Instance;
        var scenes = ds.GetScenes();

        int i = 1;
        foreach (var scene in scenes)
        {
            string filePath = dlcPath + scene.sceneName;
            string path =
#if UNITY_ANDROID
            "file://" + dlcPath + scene.sceneName;
#elif UNITY_STANDALONE_WIN || UNITY_EDITOR
	        "file://" + dlcPath + scene.sceneName;
#else
        string.Empty;
#endif

            if (File.Exists(filePath))
            {
                var download = WWW.LoadFromCacheOrDownload(path,1);
                yield return download;

                assetBundles.Add(download.assetBundle);
                sceneNames.AddRange(download.assetBundle.GetAllScenePaths());
                scene.Load = 1;
                ds.UpdateScene(scene);
            }
        }

        //UNUSED DLC
        /*
        string[] dlcFiles = Directory.GetFiles(dlcPath);
        foreach (string fileName in dlcFiles)
        {
            if (Path.GetExtension(fileName) != ".meta")
            {
                bool used = false;
                foreach (var scene in scenes)
                {
                    if (scene.sceneName.Equals(Path.GetFileName(fileName)))
                    {
                        used = true;
                        break;
                    }
                }
                if (!used)
                {
                    File.Delete(fileName);
                }
            }
        }*/

    }

    public void ShowDLC()
    {
        foreach (Transform t in allSceneContainer.transform)
        {
            Destroy(t.gameObject);
        }

        DataService ds = DataService.Instance;
        var scenes = ds.GetScenes();
        foreach (var scene in scenes)
        {
            GameObject newScene = Instantiate(allSceneItem) as GameObject;
            DLC controller = newScene.GetComponent<DLC>();
            controller.Init(scene.Name, scene.sceneName, scene.Icone);
            newScene.transform.SetParent(allSceneContainer.transform);
            
            newScene.transform.localScale = Vector3.one;
            newScene.SetActive(true);
            controller.btShowScene.AddEventListner(controller, SetScene);
        }
    }

    public void ShowMyScenesDLC()
    {
        foreach (Transform t in mySceneContainer.transform)
        {
            Destroy(t.gameObject);
        }

        DataService ds = DataService.Instance;
        var scenes = ds.GetScenes();
        foreach (var scene in scenes)
        {
            if (scene.isAvailable == 1)
            {
                GameObject newScene = Instantiate(mySceneItem) as GameObject;
                DLC controller = newScene.GetComponent<DLC>();
                controller.Init(scene.Name, scene.sceneName, scene.Icone);
                newScene.transform.SetParent(mySceneContainer.transform);

                newScene.transform.localScale = Vector3.one;
                newScene.SetActive(true);
                controller.btShowScene.AddEventListner(controller, SetScene);
            }

        }
    }

    public void SetScene(DLC dlc)
    {

        this.dlc = dlc;

        DataService ds = DataService.Instance;
        var sceneInfo = ds.GetScene(dlc.sceneName);

        GameObject newContainer = Instantiate(loadSceneContainer) as GameObject;
        SceneInfo controller = newContainer.GetComponent<SceneInfo>();
        controller.Init(sceneInfo.Name, sceneInfo.Description, sceneInfo.Load, sceneInfo.sceneName);
        newContainer.transform.SetParent(loadSceneParent.transform);
        newContainer.transform.localScale = Vector3.one;
        newContainer.SetActive(true);

        controller.buttonLoad.AddEventListner(controller, DownloadScene);
        controller.buttonBack.AddEventListner(newContainer, BackToRootContainer);

        allSceneContainer.SetActive(false);
    }

    public void BackToRootContainer(GameObject loadContainer)
    {
        Destroy(loadContainer);
        allSceneContainer.SetActive(true);
        Init();
    }

    public void DownloadScene(SceneInfo sceneInfo)
    {
        string sceneName = dlc.sceneName.Replace(".dlc", "");
        var scene = sceneNames.Find(x => x.Contains(sceneName));

        if (scene == null)
        {
            Download(sceneInfo);
        }
        else SceneManager.LoadScene(scene);
    }

    ProgressBar CreateProgressBarContainer()
    {
        GameObject newContainer = Instantiate(progressBarContainer) as GameObject;
        ProgressBar pB = newContainer.GetComponentInChildren<ProgressBar>();
        newContainer.transform.SetParent(loadSceneParent.transform);
        newContainer.transform.localScale = Vector3.one;
        newContainer.SetActive(true);
        return pB;
    }

    public void Download(SceneInfo sceneInfo)
    {
        StartCoroutine(CoDownload(sceneInfo));
    }

    IEnumerator CoDownload(SceneInfo sceneInfo)
    {
        string key = sceneInfo.keyInput.text;
        WWWForm form = new WWWForm();
        form.AddField("SceneName", dlc.sceneName);
        form.AddField("Key", key);
        form.AddField("UniqueIDDevice", SystemInfo.deviceUniqueIdentifier);
        using (WWW www = new WWW(dlc.urlSceneLoad, form))
        {
            ProgressBar pB = CreateProgressBarContainer();
            while (!www.isDone)
            {
                pB.currentAmount += www.progress * 100 / 2;
                yield return null;
            }

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogError("Log: Error DATA " + www.error);
                pB.SetErrorLog("Ошибка загрузки");
                pB.Destroy();
                yield return null;
            }

            if (www.text.Equals("Error DATA"))
            {
                Debug.LogError("Log: Error Key " + www.text.ToString());
                pB.SetErrorLog("Недействительный ключ");
                pB.Destroy();
                File.Delete(dlc.filePath);
                yield return null;
            }
            else
            {
                Debug.Log("Log: end download");
                File.WriteAllBytes(dlc.filePath, www.bytes);

                string tmpPath = Application.temporaryCachePath + "/QCAR/";
                if (!Directory.Exists(tmpPath))
                    Directory.CreateDirectory(tmpPath);

                WWWForm formDatabase = new WWWForm();
                formDatabase.AddField("SceneName", dlc.sceneName);
                WWW downloadDat = new WWW(databasePath, formDatabase);
                while (!downloadDat.isDone)
                {
                    pB.currentAmount += downloadDat.progress * 100 / 2;
                    yield return null;
                }
                if (!string.IsNullOrEmpty(downloadDat.error))
                {
                    Debug.LogError("Log: Error DATA " + downloadDat.error);
                    pB.SetErrorLog("Ошибка загрузки дополнительных данных для сцены");
                    File.Delete(dlc.filePath);
                    pB.Destroy();
                    yield return null;
                }
                else
                {

                    tmpPath += dlc.sceneName.Replace(".dlc", "") + ".zip";
                    var data = downloadDat.bytes;
                    File.WriteAllBytes(tmpPath, data);

                    ZipUtil.Unzip(tmpPath, Application.persistentDataPath + "/QCAR/");
                    Init();
                    pB.Destroy();
                    sceneInfo.Refresh();

                    ShowDLC();
                }
            }
        }


    }
}