﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneInfo : MonoBehaviour
{


    public Text sceneNameText;
    public Text sceneDescriptionText;
    public InputField keyInput;
    public Button buttonLoad;
    public Button buttonBack;

    public string sceneName;
    public int load;

    internal void Init(string name, string description, int load, string sceneName)
    {

        sceneNameText.text = name;
        sceneDescriptionText.text = description;
        this.sceneName = sceneName;
        this.load = load;
        keyInput.text = "";

        if (load == 1)
        {
            keyInput.gameObject.SetActive(false);
            buttonLoad.GetComponentInChildren<Text>().text = "Открыть";
        }
        else
        {
            keyInput.gameObject.SetActive(true);
            buttonLoad.GetComponentInChildren<Text>().text = "Скачать";
        }
    }

    public void Refresh()
    {
        keyInput.gameObject.SetActive(false);
        buttonLoad.GetComponentInChildren<Text>().text = "Открыть";
    }
}
