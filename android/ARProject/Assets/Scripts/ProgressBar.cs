﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

    public GameObject RootConainer;
    public Transform LoadingBar;
    public Transform TextIndicator;
    public Transform TextLoading;
    public Transform TextError;
    public Image Background;

    public float currentAmount;
    public float speed;

	// Use this for initialization
	void Start () {
        currentAmount = 0;
        TextError.gameObject.SetActive(false);
        Background.color = new Color32(175, 198, 244, 255);
    }
	
	// Update is called once per frame
	void Update () {
        Background.color = new Color32(175, 198, 244, 255);
        if (currentAmount < 100)
        {
            TextIndicator.GetComponent<Text>().text = ((int)currentAmount).ToString()+"%";
            TextLoading.gameObject.SetActive(true);
        }else
        {
            TextLoading.GetComponent<Text>().text = "Установка...";
            TextIndicator.GetComponent<Text>().text = "Успешно";
        }
        LoadingBar.GetComponent<Image>().fillAmount = currentAmount / 100;
	}

    public void Destroy()
    {
        //TextLoading.gameObject.SetActive(false);
        Destroy(RootConainer, 2.0f);
    }

    public void SetErrorLog(string error)
    {
        TextIndicator.GetComponent<Text>().text = "Ошибка";
        TextError.gameObject.SetActive(true);
        TextError.GetComponent<Text>().text = error;
    }

}
