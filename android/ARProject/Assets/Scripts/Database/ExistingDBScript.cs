﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ExistingDBScript : MonoBehaviour {

	public Text DebugText;

	// Use this for initialization
	void Start () {
		var ds = DataService.Instance;//("existing.db");
		//ds.CreateDB ();
		var people = ds.GetScenes ();
		ToConsole (people);

		/*people = ds.GetScenesNamed ("scene1");
		ToConsole("Searching for Roberto ...");
		ToConsole (people);

		ds.CreateScene ("");
		ToConsole("New person has been created");
		var p = ds.GetJohnny ();
		ToConsole(p.ToString());*/

	}
	
	private void ToConsole(IEnumerable<Scenes> people){
		foreach (var person in people) {
			ToConsole(person.ToString());
		}
	}

	private void ToConsole(string msg){
		DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}

}
