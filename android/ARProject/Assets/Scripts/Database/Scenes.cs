﻿using SQLite4Unity3d;

public class Scenes  {

	[PrimaryKey, AutoIncrement]
	public int Id { get; set; }
    public string sceneName { get; set; }
	public string Name { get; set; }
	public string Icone { get; set; }
	public int Load { get; set; }
    public int isAvailable { get; set; }
	public string Description { get; set; }
	public int Price { get; set; }
	public int Date { get; set; }

	public override string ToString ()
	{
		return string.Format ("[Person: Id={0}, Name={1},  Icone={2}, Load={3}, Description={4}, Price={5}, Date={6}]",
			Id, Name, Icone, Load, Description, Price, Date);
	}
}
