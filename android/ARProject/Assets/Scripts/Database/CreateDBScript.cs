﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class CreateDBScript : MonoBehaviour {

	public Text DebugText;

	// Use this for initialization
	public void Start () {
		StartSync();
	}

    private void StartSync()
    {
		var ds = DataService.Instance;
		ds.DatabaseName = "scenesDatabase.db";
        
		DebugText.text = ds.CheckDB ().ToString();
		if (!ds.CheckDB ()) {
			ds.InitDB ();
			ds.CreateDB ();

			UnityDLC uDLC = UnityDLC.Instance;
			//uDLC.GetScenes (1483228800);//1.1.2017 00:00:00

			var scenes = ds.GetScenes ();
			foreach (var scene in scenes) {
				Debug.Log ("Log: " + scene.ToString ());
			}
		
		} else {
			ds.InitDB ();
			UnityDLC uDLC = UnityDLC.Instance;
			uDLC.ShowDLC ();
		}
    }



	private void ToConsole(IEnumerable<Scenes> people){
		foreach (var person in people) {
			ToConsole(person.ToString());
		}
	}
	
	private void ToConsole(string msg){
		DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}
}
