﻿using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

public class DataService : MonoBehaviour
{

    private SQLiteConnection _connection;
    public string DatabaseName { get; set; }
    public static DataService Instance { get; private set; }

    public void Awake()
    {
        Instance = this;
    }

    public void InitDB()
    {

        Debug.Log("Log: INITDB");
#if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
		// check if file exists in Application.persistentDataPath
		var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

		if (!File.Exists(filepath))
		{
		Debug.Log("Database not in Persistent path");
		// if it doesn't ->
		// open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
		var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
		while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
		// then save to Application.persistentDataPath
		File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
		var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#elif UNITY_WP8
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);

#endif

		Debug.Log("Database written");
		}

		var dbPath = filepath;
#endif
        _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH: " + dbPath);

    }


    public bool CheckDB()
    {
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);
#if UNITY_EDITOR
#else
		if (!File.Exists (filepath)) {
			return false;
		} else {
			return true;
		}
#endif
        return false;
    }

    public void CreateDB()
    {
        _connection.DropTable<Scenes>();
        _connection.CreateTable<Scenes>();
    }

    public IEnumerable<Scenes> GetScenes()
    {
        return _connection.Table<Scenes>();
    }

    public IEnumerable<Scenes> GetScenesNamed(string SceneName)
    {
        return _connection.Table<Scenes>().Where(x => x.sceneName == SceneName);
    }

    public Scenes GetScene(string SceneName)
    {
        return _connection.Table<Scenes>().Where(x => x.sceneName == SceneName).FirstOrDefault();
    }

    public void UpdateScene(Scenes scene)
    {
        _connection.Update(scene);
    }

    public Scenes CreateScene
        (string sceneName,
        string Name,
        string Icone,
        int Load,
        int isAvailable,
        string Description,
        int Price,
        int Date)
    {
        var p = new Scenes
        {
            sceneName = sceneName,
            Name = Name,
            Icone = Icone,
            Load = Load,
            isAvailable = isAvailable,
            Description = Description,
            Price = Price,
            Date = Date
        };
        _connection.Insert(p);
        return p;
    }
}