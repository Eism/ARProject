<?php

  function download($filename) {

    if (file_exists($filename)) {
    	header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
	header("Cache-Control: public");
	header("Content-Type: application/octet-stream");
	header("Content-Transfer-Encoding: Binary");
	header("Content-Length:".filesize($filename));
        header('Content-Disposition: attachment; filename='.basename($filename));

    	ob_clean();
      	flush();
      	readfile($filename);

        //echo file_get_contents($filename); // Отдаём файл пользователю на скачивание
    }
    else print_r("Error DATA");

  }

?>
